FROM python:3

RUN apt update -q -y && apt autoremove && apt clean

WORKDIR /rlagent

# INSTALL PYTHON DEPENDENCIES #################################################

COPY . ./
RUN pip install --no-cache-dir -r requirements.txt

# INSTALL ACCENTA'S PYTHON LIBS ###############################################

RUN pip install .

#CMD pytest
